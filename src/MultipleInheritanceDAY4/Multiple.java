package MultipleInheritanceDAY4;


class Animal
{
	public void eat()
	{
		System.out.println("All Animals eat");
	}	
}
 class Dog
 {
	 public void bark() {
		 System.out.println("Dog barks");
	 }
 }
  class Cat
  {
	  public void meow()
	  {
		System.out.println("meow...meow...");  
	  }
  }

public class Multiple {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Animal a1=new Animal();
      a1.eat();
     // a1.bark();  // Multiple Inheritance is not possible in java
	}

	
	
}

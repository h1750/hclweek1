package hierarchialDAY4;

class Animal
{
	public void eat()
	{
		System.out.println("All Animals eat");
	}	
}
 class Dog extends Animal
 {
	 public void bark() {
		 System.out.println("Dog barks");
	 }
 }
  class Cat extends Animal
  {
	  public void meow()
	  {
		System.out.println("meow...meow...");  
	  }
  }

public class Hierarchial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Dog d1 = new Dog();
      d1.eat();
      d1.bark();
      Cat c1=new Cat();
      c1.eat();
      c1.meow();
	}

}

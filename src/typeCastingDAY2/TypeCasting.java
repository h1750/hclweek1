package typeCastingDAY2;

public class TypeCasting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     int i=75463;
     long l=i;      //Implicit Type casting
     float f=l;
     double d=f;
     System.out.println(i);
     System.out.println(l);
     System.out.println(f);
     System.out.println(d);
     
     double d1=78534.567;     //Explicit Type Casting
     float f1=(float)d1;
     long l1 =(long)f1;
     int i1=(int)l1;
     System.out.println(d1);
     System.out.println(f1);
     System.out.println(l1);
     System.out.println(i1);
     
	}

}

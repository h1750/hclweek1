package inHeritanceDAY4;
class Animal
{
	public void eat()
	{
		System.out.println("All animals eat");
	}
}
class Dog extends Animal
{
	public void bark()
	{
		System.out.println("Dog Barks");
	}
}
public class Inheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Dog d1=new Dog();
	      d1.eat();
	      d1.bark();
	}

}

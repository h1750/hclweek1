package constructorOverloadingDAY3;

public class ConstructorOverloading {
    String ename,color;
    double price;
    public  ConstructorOverloading (String ename,String color)
    {
    this.ename=ename;
    this.color=color;
    }
	public  ConstructorOverloading (String ename ,String color,double price)
	{
	this.ename=ename;
	this.color=color;
	this.price=price;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 ConstructorOverloading c1 = new ConstructorOverloading("maruti","blue");
		System.out.println(c1.ename +" "+c1.color);
		 ConstructorOverloading c2 = new  ConstructorOverloading("kiya","black",150000);
		 System.out.println(c2.ename + " "+ c2.color +" "+c2.price);
	}

}

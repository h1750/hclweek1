package HashcodeEqualDAY5;

import java.util.Objects;

public class Employee {

 private int id;
 private String name;
 private double salary;
 
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getSalary() {
	return salary;
}
public void setSalary(double salary) {
	this.salary = salary;
}
@Override
public int hashCode() {
	return Objects.hash(id, name, salary);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Employee other = (Employee) obj;
	return id == other.id && Objects.equals(name, other.name)
			&& Double.doubleToLongBits(salary) == Double.doubleToLongBits(other.salary);
	
}

public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
}
 
public class Client{
	public static void main(String[]args) {
		Employee emp1=new Employee();
		emp1.setId(111);
		emp1.setName("Tom");
		emp1.setSalary(50000);
		System.out.println(emp1);
	 Employee emp2=new Employee();
		emp2.setId(111);
		emp2.setName("Jerry");
		emp2.setSalary(50000);
		System.out.println(emp2);
		System.out.println(emp1.equals(emp2));
	}
}
	}


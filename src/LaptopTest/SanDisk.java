package LaptopTest;

public class SanDisk implements HardDiskInterface {
    @Override
	public double capacity() {
		return 500;
	} 
    @Override
	public String model()
	{
		return "X-2020";
	}
}

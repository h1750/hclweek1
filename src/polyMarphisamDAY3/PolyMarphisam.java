package polyMarphisamDAY3;

class A
{
public void run()
{
	System.out.println("method-1");
}
}
class B extends A
{
 public void run()
 {
	 System.out.println(" method-2"); 
 }
}

public class PolyMarphisam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A a1=new A();
		a1.run();
        B b1=new B();
         b1.run();
	    A a2=new B();
          a2.run();
	}

}
